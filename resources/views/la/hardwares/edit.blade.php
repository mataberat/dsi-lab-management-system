@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/hardwares') }}">Hardware</a> :
@endsection
@section("contentheader_description", $hardware->$view_col)
@section("section", "Hardwares")
@section("section_url", url(config('laraadmin.adminRoute') . '/hardwares'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Hardwares Edit : ".$hardware->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($hardware, ['route' => [config('laraadmin.adminRoute') . '.hardwares.update', $hardware->id ], 'method'=>'PUT', 'id' => 'hardware-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'type')
					@la_input($module, 'tgl_pembelian')
					@la_input($module, 'harga_pembelian')
					@la_input($module, 'picture')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/hardwares') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#hardware-edit-form").validate({
		
	});
});
</script>
@endpush
