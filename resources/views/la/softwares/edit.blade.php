@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/softwares') }}">Software</a> :
@endsection
@section("contentheader_description", $software->$view_col)
@section("section", "Softwares")
@section("section_url", url(config('laraadmin.adminRoute') . '/softwares'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Softwares Edit : ".$software->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($software, ['route' => [config('laraadmin.adminRoute') . '.softwares.update', $software->id ], 'method'=>'PUT', 'id' => 'software-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'type')
					@la_input($module, 'tgl_pembelian')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/softwares') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#software-edit-form").validate({
		
	});
});
</script>
@endpush
