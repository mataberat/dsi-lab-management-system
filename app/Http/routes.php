<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get("/email", function() {
//     Mail::raw('Now I know how to send emails with Laravel', function($message)
//      {
//          $message->subject('Hi There!!');
//          $message->from('admin@dsi.co.id', 'Admin DSI');
//          $message->to('akuwibonoo@gmail.com');
//      });
//  });

/* ================== Homepage + Admin Routes ================== */

require __DIR__.'/admin_routes.php';
